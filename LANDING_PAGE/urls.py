from django.conf.urls import url
from .views import index
from .views import add_activity
from .views import profile
from .views import data_json
from .views import book
from .views import registration
from .views import validasi
from .views import success
# from .views import mydiarylist
# from .views import mydiary
from django.urls import re_path

urlpatterns = [
 re_path(r'^$', index, name='index'),
 re_path('profile-page/' , profile, name='profile'),
 re_path('add_activity/', add_activity, name='add_activity'),
 re_path('book_data_json/' , data_json, name='data_json'),
 re_path('book_list', book, name='book'),
 re_path('registration', registration, name="registration"),
 re_path('validasi', validasi, name="validasi"),
 re_path('success', success, name="success"),

#  re_path('apippw/mydiary/new', mydiary,name='mydiary'),
#  re_path('apippw/mydiary/list', mydiarylist,name='mydiarylist')
]


from django.apps import AppConfig


class LandingPageConfig(AppConfig):
    name = 'LANDING_PAGE'

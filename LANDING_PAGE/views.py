from django.shortcuts import render,redirect
from .models import Diary, Registration
from .forms import form_Diary, form_Registration
from . import forms
from datetime import datetime
from django.http import HttpResponseRedirect , JsonResponse
import pytz
import json
import requests
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login

# Create your views here.
response = {}

def home(request):
	return render(request, 'home.html')
	
def index(request):
    response['form'] = forms.form_Diary
    response['diary'] = Diary.objects.all()
    html = 'new_Website.html'
    return render(request, html, response)

def profile(request):
    return render(request, 'profile_tdd.html')

def book(request): #render html buku baru
    return render(request, 'book.html')

def data_json(request):
    list_buku = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
    json_buku = list_buku.json()
    return JsonResponse(json_buku)

def add_activity(request):
    form = forms.form_Diary(request.POST)
    if form.is_valid():
        response['diary'] = request.POST['diary']
        diary = Diary(activity = response['diary'])
        diary.save()
        return HttpResponseRedirect('/landing-page/')
    else:
        form = forms.form_Diary()
    
    return render(request, 'new_Website.html', response)

def registration(request):
	response = {}
	response["forms"] = form_Registration
	return render(request, 'registration.html', response)

@csrf_exempt
def validasi(request):
	email = request.POST.get("email")
	data = {'not_valid': Subscribers.objects.filter(email__iexact=email).exists()}
	return JsonResponse(data)

def success(request):
	submited_form = form_Registration(request.POST or None)
	if (submited_form.is_valid()):
		cd = submited_form.cleaned_data
		new_subscriber = Registration(name=cd['nama'], password=cd['password'], email=cd['email'])
		new_subscriber.save()
		data = {'nama': cd['nama'], 'password': cd['password'], 'email': cd['email']}
	return JsonResponse(data)

def my_view(request):
	username = request.POST['username']
	password = request.POST['password']
	user = authenticate(request, username=username,password=password)
	if user is not None:
		requeest.session['user_login'] = username
		messages.success(request)



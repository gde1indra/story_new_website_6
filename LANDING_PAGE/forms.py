from django import forms
class form_Diary(forms.Form):
    diary = forms.CharField(widget = forms.Textarea(),max_length = 300,required = True)

class form_Registration(forms.Form):
	email_attrs = {
		'type': 'text',
		'class': 'form-control',
		'placeholder':'Example: aaaaa@gmail.com',
	}
	nama_attrs = {
		'type': 'text',
		'class': 'form-control',
		'placeholder':'Full Name',
	}
	password_attrs = {
		'type': 'password',
		'class': 'form-control',
		'placeholder':'Password',
	}
	nama = forms.CharField(max_length = 50, error_messages={"required": "Mohon masukkan nama Anda"}, widget = forms.TextInput(attrs=nama_attrs))
	password = forms.CharField(max_length = 20, min_length=8, widget = forms.PasswordInput(attrs=password_attrs), error_messages={"min_length": "password harus lebih dari atau sama dengan 8 karakter"})
	email = forms.EmailField(error_messages={"required": "Mohon masukkan email yang valid"}, max_length=50, widget=forms.TextInput(attrs=email_attrs))


from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .views import add_activity
from .views import profile
from .models import Diary
from django.utils import timezone

class Lab6Test(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('/landing-page/')
        self.assertEqual(response.status_code,200)
    def test_profile_tdd_url_is_exist(self):
        response = Client().get('/profile-page/')
        self.assertEqual(response.status_code,200)    
    def test_landing_page_using_to_do_list_template(self):
        response = Client().get('/landing-page/')
        self.assertTemplateUsed(response, 'new_Website.html')
    # def test_profile_tdd_using_to_do_list_template(self):
    #     response = Client().get('/profile-page/')
    #     self.assertTemplateUsed(response, 'lab_3.html')    
    def test_landing_page_using_index_func(self):
        found = resolve('/landing-page/')
        self.assertEqual(found.func, index)
    # def test_profile_tdd_using_index_func(self):
    #     found = resolve('/profile-page/')
    #     self.assertEqual(found.func, profile)
    def test_model_can_create_new_activity(self):
        #Creating a new activity
        new_activity = Diary.objects.create(date=timezone.now(),
        activity='Aku mau praktikum ngoding deh')
        #Retrieving all available activity
        counting_all_available_activity = Diary.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)
    # def test_can_save_a_POST_request(self):
    #     response = self.client.post('/landing-page/add_activity/',
    #     data={'date': '2017-10-12T14:14', 'activity' : 'Maen Dota Kayaknya Enak'})
    #     counting_all_available_activity = Diary.objects.all().count()
    #     self.assertEqual(counting_all_available_activity, 1)
    #     self.assertEqual(response.status_code, 302)
    #     self.assertEqual(response['location'], '/landing-page/')
    #     new_response = self.client.get('/landing-page/')
    #     html_response = new_response.content.decode('utf8')
    #     self.assertIn('Hello, Apa kabar?', html_response)

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.
class Story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver',
        chrome_options=chrome_options)
        super(Story6FunctionalTest, self).setUp()
    def tearDown(self):
        self.selenium.quit()
        super(Story6FunctionalTest, self).tearDown()
    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://ppw-c-girn-lab6.herokuapp.com/landing-page/')
        # find the form element
        title = selenium.find_element_by_id('id_diary')
        submit = selenium.find_element_by_id('submit')
        # Fill the form with data
        title.send_keys('Coba Coba')
        # submitting the form
        submit.send_keys(Keys.RETURN)
        textSource = selenium.page_source
        self.assertIn('Coba Coba',textSource)
    def test_check_centered_question(self):
        selenium = self.selenium
        selenium.get('https://ppw-c-girn-lab6.herokuapp.com/landing-page/')
        tag = selenium.find_element_by_tag_name('h1')
        self.assertEqual({'x':8,'y':21},tag.location)
    def test_check_submit_button_location(self):
        selenium = self.selenium
        selenium.get('https://ppw-c-girn-lab6.herokuapp.com/landing-page/')
        submitButton = selenium.find_element_by_id('submit')
        self.assertEqual({'x':393,'y':409},submitButton.location)
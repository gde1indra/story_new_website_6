$(document).ready(function(){
    $("#buttonUbahTema").click(function(){
        $("body").addClass("temaBackground2");
        $(".btn").addClass("temaTombol2");
        $(".accordion").addClass("temaTombol2")
        $(".table-responsive").addClass("table-responsive2")
    });
    $("#buttonTemaAwal").click(function(){
        $("body").removeClass("temaBackground2");
        $(".btn").removeClass("temaTombol2");
        $(".accordion").removeClass("temaTombol2")
        $(".table-responsive").removeClass("table-responsive2")

    });
    $("#accordionSatu").click(function(){
        $("#panel1").slideToggle("slow");
    });
    $("#accordionDua").click(function(){
        $("#panel2").slideToggle("slow");
    });
    $("#accordionTiga").click(function(){
        $("#panel3").slideToggle("slow");
    });
});

var counter = 0;
function changeStar(id){
    var star = $('#'+id).html();
    if(star.includes("gray")) {
        counter++;
        $('#'+id).html("<i class='fa fa-star' style = 'color : yellow'></i>");
        $("#jumlah_buku_favorit").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " ");
    }
    else{
        counter--;
        $('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
        $("#jumlah_buku_favorit").html("<i class='fa fa-star'style = 'color : yellow'></i> " +counter + " ");
    }   
}

$(document).ready(function(){
    $.ajax({
        url: "/landing-page/book_data_json",
        success: function(obj){
            obj = obj.items;
            $("#tabel_list_buku").append("<thead> <th> Title </th> <th> Author </th> <th> Cover </th> <th> Description </th><th> Favorite </th> </thead>");
            $("#tabel_list_buku").append("<tbody>");
            for(c = 0; c < obj.length; c++){
                var title = obj[c].volumeInfo.title;
                var author = obj[c].volumeInfo.authors;
                var image = obj[c].volumeInfo.imageLinks.thumbnail;
                var description = obj[c].volumeInfo.description;
                $("#tabel_list_buku").append("<tr><td>" + title +  "</td> <td> "+ author + "</td> <td>" + "<img src='"+ image + "'>" +"</td> <td>" + description + "</td> <td>" + 
                                    "<button class='button' style = 'background-color: Transparent; border: none' id='"+ obj[c].id + 
                                    "'onclick = 'changeStar(" +"\""+ obj[c].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>");
            }
            $("#tabel_list_buku").append("</tbody>");
        }
    });
});

// document.onreadystatechange = function(){
//     setTimeout(function(){
//         document.getElementById("load").style.visibility="hidden;"
//     },2000;)
// }
// $(document).ready(function(){
//     $("#load").fadeOut(2000,function(){
//         $("body").fadeIn(1000);
//     });
// )};

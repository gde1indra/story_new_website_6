"""NEW_WEBSITE_6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from LANDING_PAGE.views import home
# import LANDING_PAGE.urls as LANDING_PAGE

urlpatterns = [
    path('admin/', admin.site.urls),
    path('landing-page/', include(('LANDING_PAGE.urls', 'LANDING_PAGE'), namespace = 'LANDING_PAGE')),
    path('profile-page/', include(('LANDING_PAGE.urls', 'LANDING_PAGE'), namespace = 'profile_page')),
    # url(r'^login/$', auth_views.login, name='login'),
    path('login/',auth_views.LoginView.as_view(template_name="login.html"), name="login"),
    path('logout/',auth_views.LogoutView.as_view(template_name="login.html"), name="logout"),
    # path(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    url(r'^$', home, name='home'),
    
]
